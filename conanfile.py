from conans import ConanFile, CMake, tools
from conans.tools import Version, OSInfo
import os
import errno
import glob
import shutil
import subprocess

def _force_symlink(src, dst):
    try:
        os.symlink(src, dst)
    except OSError as e:
        if e.errno == errno.EEXIST:
            os.remove(dst)
            os.symlink(src, dst)
        else:
            raise e

class ARCore(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = 'cmake', 'cmake_find_package'

    default_options = {
                       # ceres
                       'ceres-solver:shared': False,
                       'ceres-solver:fPIC': True,
                       'ceres-solver:use_gflags': True,
                       'ceres-solver:use_glog': True,
                       'suitesparse:blas': 'openblas',
                       'openblas:build_lapack': True,
                       'openblas:dynamic_arch': True,  # as otherwise default is skylake and will fail on CI
                       'openblas:use_thread': False, # ceres_solver docs say to turn threading off in openblas
                       'openblas:shared': True,
                       'glog:with_threads': True,
                       'glog:with_gflags': True,
                       'gflags:nothreads': False,

                    #    'pango:shared': True,
                    #    'cairo:shared': True,

                       'libzip:crypto': False,

                       # civetweb's defaults cause stack ovf when linking w/suitesparse
                       # see https://stackoverflow.com/questions/60967866/stack-overflow-when-adding-link-dependency-available-stack-reduced-with-linking
                       'civetweb:thread_stack_size': 'pthread_default',

                       'zlib:shared': True,

                       'boost:without_test': True,
                       'boost:shared': True,

                       'sqlite3:shared': True,

                       'freeimage:shared': True,
                       'freeimage:with_jpeg2000': False,
                       'freeimage:with_openexr': False,
                       'freeimage:with_webp': False,
                       'freeimage:with_jxr': False,
                       'freeimage:with_raw': False,
                       'freeimage:with_tiff': False,

                       'glew:shared': True

    }

    def build_requirements(self):
        pack_names = []
        if tools.os_info.linux_distro == "ubuntu":
            pack_names.append('patchelf')  # needed to remove rpath from libs in lib/

        installer = tools.SystemPackageTool()
        for pkg in pack_names:
            installer.install(pkg)

    def requirements(self):
        os_info = OSInfo()

        self.requires('glog/0.5.0')
        self.requires('ceres-solver/2.0.0')
        if self._is_tegra() and os_info.linux_distro == 'ubuntu' and os_info.os_version == '18.04':
            # For Xavier, because we run w/ubuntu 18.04 and probably an old assembler we run into
            # https://github.com/xianyi/OpenBLAS/issues/4121
            # So we use an older version for the moment
            self.requires('openblas/0.3.20')
        else:
           self.requires('openblas/0.3.23')


        self.requires("eigen/3.4.0")
        self.requires("sqlite3/3.42.0")
        self.requires("gtest/1.13.0")
        self.requires("flann/1.9.2")

        # self.requires("freeimage/3.18.0")
        self.requires("metis/5.1.1")
        # self.requires("suitesparse/5.12.0")
        self.requires("suitesparse/5.2.0@invisionai_oss/stable")

        self.requires("zlib/1.2.13")

        self.requires("spdlog/1.10.0")
        self.requires("nlohmann_json/3.11.2")
        self.requires("boost/1.79.0")
        self.requires("zstd/1.5.0")
        self.requires("freeimage/3.18.0")
        self.requires("glew/2.2.0")
        self.requires("lz4/1.9.4")
        self.requires("cgal/5.5.1")

    def _is_tegra(self) -> bool:
        """ Returns true if system is running on tegra/xavier """
        return os.path.isfile('/etc/nv_tegra_release')




    def _imports_fix_rpaths(self):
        # remove rpaths so our binary can always find the libs
        # (see compiler.cmake for info about how we set rpath for our apps)
        for lib_name in glob.glob('lib/*.so*'):
            self.run('patchelf --remove-rpath ' + lib_name)
            self.run('patchelf --set-rpath "\$ORIGIN" ' + lib_name)

        # fix rpath for gstreamer plugins, or they won't be able to find the .so files they need
        for lib_name in glob.glob('lib/gstreamer-1.0/*.so*'):
            self.run('patchelf --set-rpath "\$ORIGIN/../" ' + lib_name)


    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.so*", dst="lib", src="lib")

